# -*- coding: utf-8 -*-
"""
Created on Wed May 15 12:40:22 2019

@author: cgross

Figure 4: Histogram of pPHRED score distribution of (pre-)miRNA transcripts and their surrounding up- and downstream regions.
"""

import os,sys
import numpy as np
import matplotlib.pyplot as plt 
import scipy.stats
from statsmodels.sandbox.stats.multicomp import multipletests


#Computes the probability of superiority or the ROC-AUC, the liklihood that one group has more often a larger value than the other group.Input are the sizes of the two populations and the computed U statistic from the Mann-Whiteny U test
def prob_superiority(U,size_a,size_b):
    return U/(size_a*size_b)

def read_file(file_iter):
    out_dict = {}
    for lines in file_iter:
        line = lines.strip().split('\t')
        if line[-1]==',':
            line.pop()
        if len(line)>1: #if the length is still larger than 1 it must have some values, therefore store them otherwise replace them by 0 which would be neutral according to PhyloP25
            scores = line[1].split(',')
        else:
            scores = ['0']
        scores = list(filter(None, scores)) #remove empty strings, which may be the case when there are more comma then values
        out_dict[line[0]] = np.array(list((map(float, scores))))
    return out_dict

def dict_to_list(inut):
    output = []
    for inputkeys in inut.keys():
        output.extend(inut[inputkeys])
    return output

def plot_histo(miRNA,up_down,log=True):
    plt.hist(miRNA, color = 'red', log=log, alpha = 0.7, bins = 150)
    plt.hist(up_down, color = 'blue', log=log,alpha = 0.7, bins = 150)
    plt.axvline(x=np.mean(miRNA),color='red',alpha=0.7)
    plt.axvline(x=np.mean(up_down),color='blue',alpha=0.7)
    if log:
        U,pvalue = scipy.stats.mannwhitneyu(miRNA,up_down,alternative='greater')
        plt.title('Log-Histogram of pCADD-scores One-tailed U-test pValue: %s, CLES: %.4f'%(pvalue,prob_superiority(U,len(miRNA),len(up_down))))
        plt.ylabel('log-Counts')
    else:
        U,pvalue = scipy.stats.mannwhitneyu(miRNA,up_down,alternative='greater')
        plt.title('Histogram of pCADD-scores One-tailed U-test pValue: %s, CLES: %.4f'%(pvalue,prob_superiority(U,len(miRNA),len(up_down))))
        plt.ylabel('Counts')
    plt.legend(('miRNA','Up&Down'))
    plt.xlabel('pCADD-Scores, miRNA mean: %.3f, Up&Down mean: %.3f'%(np.mean(miRNA),np.mean(up_down)))



#Reading in the files containing the max-pPHRED scores for miRNAs that were not used in the training set and their up&downstream regions. 
#the scores are stored in a dictionary with the annotated region (chr:start:end) as key and as value a list with all max-pPHRED scores for that region.
miRNA_dict = read_file(open('max-pPHRED-annotated_miRNAs-regions.tsv','r'))
updown_dict = read_file(open('max-pPHRED-annotated_up-down-regions.tsv','r'))

#joining all values of the dictonaries into one list
miRNA_list = dict_to_list(miRNA_dict)
updown_list = dict_to_list(updown_dict)

#activating the IPython magic for matplotlib
%matplotlib
#potting histogram of pPHRED scores for regions coding for miRNAs and their immediate up-&downstream regions.
plot_histo(miRNA_list,updown_list,False)





"""
#this function plots the differences between the histogram bins, this plot is not in the manuscript

#create fix sets of bins (150 bins)
def scatter_diff(miRNA,up_down):
    #determining who has the largest PHRED score
    if np.max(miRNA)>np.max(up_down):
        max_phred = np.max(miRNA)
    else:
        max_phred = np.max(up_down)
    
    #obtaining the counts per bin
    miRNA_binned = np.histogram(miRNA, bins=np.linspace(0,max_phred,150))[0]
    updown_binned = np.histogram(up_down, bins=np.linspace(0,max_phred,150))[0]
    
    #computing the differences in counts and creating a 2D array with differences in 1 row and bins in the other.
    difference = miRNA_binned-updown_binned
    diff_histo = np.array([difference,np.linspace(0,max_phred,149)])
    
    #plotting first the values above 0 and then the values below 0, accessting everytime only the count and bin for which either of the two conditions are met.
    plt.scatter(diff_histo[:,diff_histo[0,:]>0][1,:],diff_histo[:,diff_histo[0,:]>0][0,:],alpha = 0.7,color='red')
    plt.scatter(diff_histo[:,diff_histo[0,:]<=0][1,:],diff_histo[:,diff_histo[0,:]<=0][0,:],alpha = 0.7,color='blue')
    plt.legend(('counts (miRNA>Up&Down)','counts (miRNA<Up&Down)'))
    
    #plot 0 line and annotated plot
    plt.axhline(y=0,linewidth=1, color='black')
    plt.title('Scatterplot of the histogram differences')
    plt.ylabel('difference in Counts')
    plt.xlabel('binned PHRED-Scores')
    plt.xticks(range(0,int(max_phred),int((int(max_phred)+1.)/10)))
    plt.grid()



scatter_diff(miRNA_list,updown_list)
"""
