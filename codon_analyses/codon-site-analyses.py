# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 12:35:55 2019

@author: cgross

#Figure 2 and 3 of the manuscript Christian Groß et al. pCADD: SNV prioritisation in Sus scrofa
"""


import os,sys
import numpy as np
import pickle
from statsmodels.sandbox.stats.multicomp import multipletests
import scipy.stats 
import matplotlib.pyplot as plt 


"""
#Figure 2, Counts of significant p-values between the three different positions in a codon
"""


#loading the test results, each position against each other position as boolean values
corrected_ustats_third_second = np.genfromtxt('third-second-significant-tests.csv',delimiter=',')==1
corrected_ustats_third_first = np.genfromtxt('third-first-significant-tests.csv',delimiter=',')==1
corrected_ustats_second_third = np.genfromtxt('second-third-significant-tests.csv',delimiter=',')==1
corrected_ustats_first_third = np.genfromtxt('first-third-significant-tests.csv',delimiter=',')==1
corrected_ustats_second_first = np.genfromtxt('second-first-significant-tests.csv',delimiter=',')==1
corrected_ustat_first_second = np.genfromtxt('first-second-significant-tests.csv',delimiter=',')==1


"""
#checking the number of significant pvalues.
corrected_ustats_third_second.sum() #3<2 8901
corrected_ustats_third_first.sum() #3<1 8830
corrected_ustats_second_third.sum() #2<3 340
corrected_ustats_first_third.sum() #1<3 189
corrected_ustats_second_first.sum() #2<1 766
corrected_ustat_first_second.sum() #1<2 3066
"""

#activating the IPython magic for matplotlib
%matplotlib

#Joining the counts of all significant tests in one list
all_values = [corrected_ustats_third_second.sum() ,corrected_ustats_second_third.sum(), corrected_ustats_third_first.sum() ,corrected_ustats_first_third.sum() , corrected_ustats_second_first.sum() ,corrected_ustat_first_second.sum()]


#creating barplots with the number of tests, resulting in a corrected, significant pvalue. 
fig, ax = plt.subplots()
width = 0.35         # the width of the bars
ind = np.array([0,0+width,1,1+width,2,2+width])
bars = []
for i in range(len(ind)):
    bars.append(ax.bar(ind[i], all_values[i], width, bottom=0))
    height = bars[-1][0].get_height()
    ax.text(bars[-1][0].get_x() + bars[-1][0].get_width()/2., 1.05*height,'%i' % int(height), ha='center', va='bottom')

ax.set_title('Number rejected null-hypothesis')
ax.set_xticks(ind)
ax.set_xticklabels(('3rd position < 2nd position','3rd position > 2nd position','3rd position < 1st position','3rd position > 1st position','2nd position < 1st position','2nd position > 1st position'))
ax.yaxis.grid(True)
fig.autofmt_xdate()
ax.autoscale_view()


"""
#Figure 3, Histogram of effect sizes measured in CohenD coefficients for the effect sizes between the pPHRED scores of the three codon positions
"""

#loading the pre-computed cohend values pPHRED scores of each position, 
CLES_3_2 = np.genfromtxt('CLES_3-2.csv',delimiter=',')
CLES_3_1 = np.genfromtxt('CLES_3-1.csv',delimiter=',')
CLES_2_1 = np.genfromtxt('CLES_2-1.csv',delimiter=',')

#plotting the cohend values as histograms as histograms
plt.hist([CLES_2_1,CLES_3_2,CLES_3_1],log=False,color=['purple','green','yellow'],histtype='stepfilled',alpha = 0.7, lw=2,edgecolor='black' ,stacked=False,bins =200) 
plt.legend(('CLES 3-1','CLES 3-2','CLES 2-1')) #backwards
plt.title('Histogram of CohenD values') 
plt.xlabel('CLES \n CLES 2-1 mean:%.3f \n CLES 3-2 mean:%.3f \n CLES 3-1 mean:%.3f'%(np.mean(CLES_2_1),np.mean(CLES_3_2),np.mean(CLES_3_1)))
plt.axvline(x=np.mean(CLES_3_2),color='green')
plt.axvline(x=np.mean(CLES_3_1),color='yellow')
plt.axvline(x=np.mean(CLES_2_1),color='purple')
plt.xticks(list(np.arange(0,1.1,0.1)))
plt.grid(axis='y')
