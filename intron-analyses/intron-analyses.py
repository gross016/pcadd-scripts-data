# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 15:17:34 2019

@author: cgross
Figure 5: pPHRED scores per intron compared to all other introns, for the first 20 introns
"""

import pickle
import numpy as np
import matplotlib.pyplot as plt 



#loading dictionaries with the intron numbers as keys and as values integers, 
#the value is the number of tests conducted for a particular intron
with open('number_tests.pickle', 'rb') as f:
    counts_tests = pickle.load(f,encoding='latin1')

#the value is the number of tests that returned a significant result after correction
with open('number_corrected-significant-tests.pickle', 'rb') as f:
    counts_significant_dict = pickle.load(f,encoding='latin1')


num_tests_list = []
num_significant_list = []
#converting the dictionaries to lists
for i in counts_tests.keys():
    num_tests_list.append(counts_tests[i])
    num_significant_list.append(counts_significant_dict[i])


#IPython magic for matplotlib    
%matplotlib

#plotting barplots of the number of tests and the number of rejected null hypotheses and the ratio between both with a second scale on the right hand side
fig, ax1 = plt.subplots()
ax1.bar(list(range(1,21)), num_tests_list[:20], align='center', alpha=0.5, ecolor='black', capsize=10)
ax1.bar(list(range(1,21)), num_significant_list[:20], align='center', alpha=0.5, ecolor='black', capsize=10)
ax1.set_xticks(list(range(1,21)))
ax1.legend(('number tests','number significant'))
ax1.set_xlabel('Intron Number')
ax1.set_ylabel('Counts')
ax2 = ax1.twinx()  # initiate a second axes that shares the same x-axis
values_plot = np.array(((np.array(num_significant_list)[:20])/np.array((num_tests_list)[:20])))
ax2.plot(np.append(np.nan,values_plot),color='red')
ax2.set_ylabel('Ratio, (significant)/(all tests)')
ax2.legend(('Ratio'),loc=1)

