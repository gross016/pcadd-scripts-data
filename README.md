# pCADD-scripts-data

Behind codon-analyses, intron-analyses and miRNA-analyses are scripts and data to reproduce the figures from the manuscript Christian Groß et al. pCADD: SNV prioritisation in Sus scrofa
Each script was tested in Python 3.7, to visualize the figures, the scripts make use of IPython magic.


The scripts pCADD_annotate_sequence.py and pCADD_annotate_SNP.py can be used to annotate entire sequences with pCADD summary scores and pCADD_annotate_SNP.py can be used to annotate individual SNPs with their respective pCADD score or to retrieve all scores for all substitutions for a particular site. Both scripts are tested in Python 2.7.4,  return their output to standard out and make use of the sys,argparse and pysam modules. The infiles used in the examples below can be downloaded from these websites: http://www.bioinformatics.nl/pCADD/indexed_pPHRED-summary-scores/
http://www.bioinformatics.nl/pCADD/indexed_pPHRED-scores/


pCADD_annotate_sequence.py reads in an input file line by line and extracts the genomic location from the columns specified in the -c parameter. It writes the line of the input file and attaches the scores of the specified sequences at the end of the line. If a header is specified, it will first generate a header and gives the new column the value that is specified in the -s parameter. This script requires that the .tsv.gz data file and the .tsv.gz.tbi index file are in the same folder.

**python pCADD_annotate_sequence.py -i test_infile_annotate-sequence.tsv -c 1,2,3 -p 18_max-PHRED-pCADD.tsv.gz -s max-Score**


pCADD_annotate_SNP.py reads in an input file line by line and extracts the genomic location of the site of interest for which all scores and all substitutions are returned. The first two values of the -c parameter must contain the chromosome and position of interest. If the value for a specific substitution shall be returned, the third and fourth value must be the indexes of the Reference and Alternative alleles. The output is always in the format Chromosome, Position, Reference, Alternative, pCADD score. These values are delimited by the delimite specified with -d.  This script requires that the .tsv.gz data file and the .tsv.gz.tbi index file are in the same folder.

**python pCADD_annotate_SNP.py -i test_SNP-annotate.vcf -c 1,2 -p 10_pCADD-PHRED-scores.tsv.gz**


**python pCADD_annotate_SNP.py -i test_SNP-annotate.vcf -c 1,2,4,5 -p 10_pCADD-PHRED-scores.tsv.gz**



"Miscellaneous_dump.zip" are additional scripts used in the generation of features, variant annotations and training. They are not further prepared and not arranged as a pipeline.
