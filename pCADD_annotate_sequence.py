# -*- coding: utf-8 -*-
"""
Created on Wed Feb 06 2019

@author: Christian Gross

python 2.7.4 linux-64
Output is written to standard out
"""

import sys
import pysam
import argparse


def retrieve_score(pysam_iter):
	scores = []
	for entries in pysam_iter:
		scores.append(entries.strip().split('\t')[-1])
	
	return scores

def check_header(i):
	try:
		int(i)
		return False
	except ValueError:
		return True

def write_header(file_handle,position,delimiter,label):
	potential_header = file_handle.next().strip().split(delimiter)
	#checks if the potential_header is actually a header, if yes then it adds another label at the end which is specified in the input parameters
	if check_header(potential_header[position]):
		sys.stdout.write(delimiter.join(potential_header)+delimiter+label+'\n')
	else:
		file_handle.seek(0)

parser = argparse.ArgumentParser(description='')
parser.add_argument("-i", "--infile", dest="infile", help="file containing the locations for which pCADD scores shall be retrieved.", default='')
parser.add_argument("-d", "--delimiter", dest="delim", help="delimiter used to separate different columns in infile. (default = \t)",default='\t')
parser.add_argument("-c", "--column", dest="columns", help="In which columns of the infile can the chromosome, start and end position be found for which pCADD scores shall be retrieved. These have to be three comma seperated numbers, indicating the (1-based) indexes of the chromosome, start and end. The first number is always the chromosome, the second the start and the third number is indicating the column containing the end location. (def 1,2,3)", default='1,2,3')
parser.add_argument("-p", "--pCADDfile", dest="pCADDfile", help="file containing the pCADD phred-like scores, tab delimited, bgziped and tabix indexed, scores are the last column", default='')
parser.add_argument("-s", "--score_label", dest="score_label", help="Specifies the header label for the scores which you want to retrieve. This is only necessary when a header is already available at the beginning of the infile, otherwise this parameter is ignored. (def Score", default='Score')
args = parser.parse_args()


pCADD_file = pysam.Tabixfile(args.pCADDfile)
infile = open(args.infile,'r')


#-1 because these are column indexes and python is 0-based
location = args.columns.strip().split(',')
if len(location)==3:
	chr,start,end = int(location[0])-1,int(location[1])-1,int(location[2])-1
	write_header(infile,start,args.delim,args.score_label)
else:
	sys.exit('Genomic location cannot be established, the handed over argument is following\n %s'%args.columns)


for lines in infile:
	line = lines.strip().split(args.delim)
	
	phred_list = retrieve_score(pCADD_file.fetch(line[chr],int(line[start]),int(line[end])))
	
	if len(phred_list)!=0:
		#this writes out only the list of scores
		sys.stdout.write(lines.strip()+args.delim+','.join(phred_list)+'\n')
	else:
		#this replaces the list of scores with dash
		sys.stdout.write(lines.strip()+args.delim+'-'+'\n')
