# -*- coding: utf-8 -*-
"""
Created on Wed Feb 06 2019

@author: Christian Gross

python 2.7.4 linux-64

Output is written to standard out
"""

import os,sys
import pysam
import argparse
import numpy as np


def retrieve_score(pysam_iter,ref='X',alt='X'):
	if ref==alt:
		for entries in pysam_iter:
			sys.stdout.write(args.delim.join(entries.strip().split('\t'))+'\n')
	else:
		ref = ref.upper()
		alt = alt.upper()
		switch = True
		for entries in pysam_iter:
			entry = entries.strip().split('\t')
			if (entry[2] == ref) and (entry[3] == alt):
				sys.stdout.write(args.delim.join(entry)+'\n')
				switch = False
				break
		if switch:
			sys.stdout.write("Warning scores for requested alleles %s could not be found at the location %s \n" %(','.join([ref,alt]),':'.join([entry[0],entry[1]])))


def write_header(file_handle,position,delimiter):
	sys.stdout.write(delimiter.join(["#Chrom","Position","Reference","Alternative","pCADD"])+'\n')


parser = argparse.ArgumentParser(description='')
parser.add_argument("-i", "--infile", dest="infile", help="file containing the locations for which pCADD scores shall be retrieved.", default='')
parser.add_argument("-d", "--delimiter", dest="delim", help="delimiter used to separate different columns in infile. (default = \t)",default='\t')
parser.add_argument("-c", "--column", dest="columns", help="in which columns of the infile can the genomic locations, Reference allele and the Alternative allele be found. Two or four numbers have to be handed over that are comma separated. The first two numbers are always the column indexes(1-based) of the the chromosome and genomic position. If four numbers are handed over the third and fourth number are the column indexes(1-based) of the Reference and Alternative alleles, respectievely. If only two numbers are handed over all values for all substitutions are returned.", default='')
parser.add_argument("-p", "--pCADDfile", dest="pCADDfile", help="file containing the pCADD phred-like scores, tab delimited, bgziped and tabix indexed, scores are the last column", default='1_pCADD-PHRED-scores.tsv.gz')
args = parser.parse_args()


pCADD_file = pysam.Tabixfile(args.pCADDfile)
infile = open(args.infile,'r')


#-1 because these are column indexes and python is 0-based
location = args.columns.strip().split(',')
if len(location)==2:
	chr_ind,pos_ind = int(location[0])-1,int(location[1])-1
elif len(location)==4:
	chr_ind,pos_ind,Ref_ind,Alt_ind = int(location[0])-1,int(location[1])-1,int(location[2])-1,int(location[3])-1
else:
	sys.exit('Columns specifying the genomic location cannot be identified, these need to be either 2 or 4 comma seperated numbers, the handed over argument is following\n %s'%args.columns)

#write header to sys.out
write_header(infile,pos_ind,args.delim)

for lines in infile:
	#All preceding lines starting with # are irgnored
	if lines.startswith('#'):
		continue
	
	line = lines.strip().split(args.delim)
	if len(location)==2:
		retrieve_score(pCADD_file.fetch(line[chr_ind],int(line[pos_ind])-1,int(line[pos_ind])))
	else:
		retrieve_score(pCADD_file.fetch(line[chr_ind],int(line[pos_ind])-1,int(line[pos_ind])),line[Ref_ind],line[Alt_ind])

